package edu.uprm.cse.datastructures.cardealer;


import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
@Path("/cars")
public class CarManager {
	private final long carId;
	private final String carBrand;
	private final String carModel;
	private final String carModelOption;
	private final int carPrice;
    private static CircularSortedDoublyLinkedList<Car> carList= new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	private static final AtomicLong counter = new AtomicLong(100);

	private CarManager(CarManagerBuilder builder){
		this.carId = builder.carId;
		this.carBrand = builder.carBrand;
		this.carModel = builder.carModel;
		this.carModelOption = builder.carModelOption;
		this.carPrice = builder.carPrice;

	}

	public CarManager(){
		CarManager car = new CarManager.CarManagerBuilder().id().build();
		this.carId = car.getId();
		this.carBrand = car.getCarBrand();
		this.carModel = car.getCarModel();
		this.carModelOption = car.getCarModelOption();
		this.carPrice = car.getCarPrice();

	}

	public CarManager(long id, String carBrand, String carModel,
			String carModelOption, int carPrice){
		CarManager car = new CarManager.CarManagerBuilder().id()
				.CarBrand(carBrand)
				.CarModel(carModel)
				.CarModelOption(carModelOption)
				.CarPrice(carPrice)

				.build();
		this.carId = car.getId();
		this.carBrand = car.getCarBrand();
		this.carModel = car.getCarModel();
		this.carModelOption = car.getCarModelOption();
		this.carPrice = car.getCarPrice();

	}

	public long getId(){
		return this.carId;
	}

	public String getCarBrand() {
		return this.carBrand;
	}

	public String getCarModel() {
		return this.carModel;
	}

	public String getCarModelOption(){
		return this.carModelOption;
	}

	public int getCarPrice() {
		return this.carPrice;
	}



	@Override
	public String toString(){
		return "ID: " + carId 
				+ " Car Brand: " + carBrand
				+ " Car Model: " + carModel + "\n"
				+ "Car Model Option: " + carModelOption + "\n"
				+ "Car Price: " + carPrice;

	}  

	public static class CarManagerBuilder{
		private long carId;
		private String carBrand = "";
		private String carModel = "";
		private String carModelOption = "";
		private int carPrice = (Integer) 0;


		public CarManagerBuilder id(){
			this.carId = CarManager.counter.getAndIncrement();
			return this;
		}

		public CarManagerBuilder CarBrand(String carBrand){
			this.carBrand = carBrand;
			return this;
		}

		public CarManagerBuilder CarModel(String carModel){
			this.carModel = carModel;
			return this;
		}

		public CarManagerBuilder CarModelOption(String carModelOption){
			this.carModelOption = carModelOption;
			return this;
		}

		public CarManagerBuilder CarPrice(int carPrice){
			this.carPrice = carPrice;
			return this;
		}





		public CarManager build(){
			return new CarManager(this);
		}
		

	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		carList.add(car);
		return Response.status(201).build();
	}
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		for(int i=0; i<carList.size();i++){
			if(carList.get(i).getCarId()==id){
				return carList.get(i);
			}
		}
		JsonError j= new JsonError("Error", "Car " + id + " not found");
		throw new NotFoundException(j);
		
	}
	
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars(){
		Car[] cars=new Car[carList.size()];
		
		for(int i=0; i<cars.length;i++){
			cars[i]=carList.get(i);
			
		}
		
		return cars;
	}
	
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		for(int i=0; i<carList.size();i++){
			if(carList.get(i).getCarId()==car.getCarId()){
				carList.remove(i);
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	@DELETE
	@Path("{id}/delete")
	public Response deleteCar (@PathParam("id") long id){
		for(int i=0; i<carList.size(); i++){
			if(carList.get(i).getCarId()==id){
				if(carList.remove(i)){
					return Response.status(Response.Status.OK).build();
				}
			
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
		
	}

}
