package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;






public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private DNode<E> header, current;
	private int length; 
	private Comparator<E> comparator;

	public CircularSortedDoublyLinkedList(Comparator<E> comparator) { 
		header = new DNode<E>();
		header.setNext(header);
		header.setPrev(header);


		length = 0;
		this.comparator=comparator;
	}
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(E obj) {
		if (obj == null)
			return false; 
		DNode<E> node = new DNode<E>(obj);
		DNode<E> current = this.header.getNext();
		while (current != this.header && this.comparator.compare(current.getElement(), node.getElement()) < 0) {
			current = current.getNext();
		}
		node.setNext(current);
		node.setPrev(current.getPrev());
		node.getPrev().setNext(node);
		current.setPrev(node);
		this.length++;
		return true;
	}


@Override
public int size() {
	// TODO Auto-generated method stub
	return length;
}
private void cleanLinks(DNode<E> obj){
	obj.getPrev().setNext(obj.getNext());
	obj.getNext().setPrev(obj.getPrev());
	obj.setNext(null);
	obj.setPrev(null);
	length--;
}

@Override
public boolean remove(E obj) {
	// TODO Auto-generated method stub
	current=header.getNext();

	while(current!=header){
		if(current.getElement().equals(obj)){
			cleanLinks(current);
			return true;
		}
		current=current.getNext();
	}
	return false;
}

@Override
public boolean remove(int index) {
	// TODO Auto-generated method stub
	if(index<0|| index>length){
		throw new IndexOutOfBoundsException("Index is not within the size");
	}
	int counter=0;
	DNode<E> current=header.getNext();

	while(current.getNext() != header){
		if(counter==index){
			cleanLinks(current);

			return true;
		}

		counter++;
		current=current.getNext();
	}
	return false;
}

@Override
public int removeAll(E obj) {
	// TODO Auto-generated method stub
	current=header.getNext();
	int counter= 0;
	while(current!=header){
		remove(obj);
		current=current.getNext();
	}
	return counter;
}

@Override
public E first() {
	// TODO Auto-generated method stub
	return this.isEmpty() ? null:header.getNext().getElement();
}

@Override
public E last() {
	// TODO Auto-generated method stub
	return this.isEmpty() ? null: header.getPrev().getElement();
}

@Override
public E get(int index) {
	// TODO Auto-generated method stub
	if(index < 0 || index > length){
		throw new IndexOutOfBoundsException("Index is not within the range");
	}
	int counter=0;
	DNode<E> helper = header.getNext();

	while(helper != header){
		if(counter == index){
			return helper.getElement();
		}
		counter++;
		helper=helper.getNext();
	}
	return null;
}

@Override
public void clear() {
	// TODO Auto-generated method stub
	header.setNext(header);
	header.setPrev(header);
	length=0;
}

@Override
public boolean contains(E e) {
	// TODO Auto-generated method stub
	if(e==null || this.size()==0) return false;
	
current=header.getNext();
	
	for(int i=0; i<this.size();i++){
		if(current.getElement().equals(e)){
             return true;			
		}
		current=current.getNext();
	}

	return false;
}

@Override
public boolean isEmpty() {
	// TODO Auto-generated method stub
	return length == 0;
}

@Override
public int firstIndex(E e) {

	current=header.getNext();
	
	for(int i=0; i<this.size();i++){
		if(current.getElement().equals(e)){
             return i;			
		}
		current=current.getNext();
	}

	return -1;
}

@Override
public int lastIndex(E e) {

current=header.getPrev();
	
	for(int i=this.size()-1; i>0;i--){
		if(current.getElement().equals(e)){
             return i;			
		}
		current=current.getPrev();
	}

	return -1;
}

private static class DNode<E> implements Node<E> {
	private E element; 
	private DNode<E> prev, next; 

	// Constructors
	public DNode() {}

	public DNode(E e) { 
		element = e; 
	}

	public DNode(DNode<E> p, E e, DNode<E> n) { 
		prev = p; 
		next = n; 
		element=e;
	}

	// Methods
	public DNode<E> getPrev() {
		return prev;
	}
	public void setPrev(DNode<E> prev) {
		this.prev = prev;
	}
	public DNode<E> getNext() {
		return next;
	}
	public void setNext(DNode<E> next) {
		this.next = next;
	}
	public E getElement() {
		return element; 
	}

	public void setElement(E data) {
		element = data; 
	} 


	public void cleanLinks() { 
		prev = next = null; 
	}

}

}
